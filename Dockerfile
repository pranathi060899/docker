FROM ubuntu:latest
RUN apt-get update && apt-get install -y git
WORKDIR /app
RUN git init && git clone https://gitlab.com/pranathi060899/docker.git
FROM maven:latest AS build
WORKDIR /app

# Copy the source code to the container
COPY . .

# Build the Maven project
RUN mvn clean install



